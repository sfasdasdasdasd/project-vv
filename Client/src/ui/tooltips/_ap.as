﻿




package ui.tooltips {

    import com.company.project_v.objects.GameObject;
    import com.company.utils.SimpleText;

    import flash.filters.DropShadowFilter;

    import ui.elements._gV_;

    public class _ap extends _for_ {

    public function _ap(_arg1:GameObject) {
        super(6036765, 1, 16549442, 1, false);
        this.text_ = new SimpleText(22, 16549442, false, 0, 0, "Myriad Pro");
        this.text_.setBold(true);
        this.text_.text = "Quest!";
        this.text_.updateMetrics();
        this.text_.filters = [new DropShadowFilter(0, 0, 0)];
        this.text_.x = 0;
        this.text_.y = 0;
        addChild(this.text_);
        this._id = new _gV_(0xB3B3B3, true, _arg1);
        this._id.x = 0;
        this._id.y = 32;
        addChild(this._id);
        filters = [];
    }
    public var _id:_gV_;
    private var text_:SimpleText;
}
}//package ui.tooltips

