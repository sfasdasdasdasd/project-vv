/**
 * Created by vooolox on 24-2-2016.
 */
package ui.fonts {

  import mx.core.FontAsset;

  [Embed(source="Hack-Bold.ttf", fontName="Hack Bold", fontFamily="Mono", fontStyle="Bold", embedAsCFF="true", mimeType="application/x-font-truetype")]
  public class Hack_Bold_CFF extends FontAsset {

    public function Hack_Bold_CFF() {
      super();

    }
  }
}
