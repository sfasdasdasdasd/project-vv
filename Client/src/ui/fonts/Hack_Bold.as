/**
 * Created by vooolox on 24-2-2016.
 */
package ui.fonts {

  import mx.core.FontAsset;

  [Embed(source="Hack-Bold.ttf", fontName="Hack Bold", fontFamily="Mono", fontStyle="Bold", embedAsCFF="false", mimeType="application/x-font-truetype")]
  public class Hack_Bold extends FontAsset {

    public function Hack_Bold() {
      super();

    }
  }
}
