﻿




package _U_5 {

    import _sp.Signal;

    import networking.packets.server.MapInfo;

    public class _D_L_ extends Signal {

    private static var instance:_D_L_;

    public static function getInstance():_D_L_ {
        if (!instance) {
            instance = new (_D_L_)();
        }
        return (instance);
    }

    public function _D_L_() {
        super(MapInfo);
        instance = this;
    }

}
}//package _U_5

