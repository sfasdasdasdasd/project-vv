﻿




package _zD_ {

    import _C__._cM_;

    import _U_5._dd;

    import ui.views.CreditsView;
    import ui.views.TitleView;

    public class _0F_Q_ extends _cM_ {

    [Inject]
    public var view:CreditsView;
    [Inject]
    public var _T__:_dd;

    override public function initialize():void {
        this.view.close.add(this._0H_N_);
        this.view.initialize();
    }

    override public function destroy():void {
        this.view.close.remove(this._0H_N_);
    }

    private function _0H_N_():void {
        this._T__.dispatch(new TitleView());
    }

}
}//package _zD_

