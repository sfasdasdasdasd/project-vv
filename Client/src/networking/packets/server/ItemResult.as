﻿




package networking.packets.server {

    import flash.utils.IDataInput;

    public class ItemResult extends _01Q_ {

    public function ItemResult(_arg1:uint) {
        super(_arg1);
    }

    public var item_:int;
    public var data_:String;

    override public function parseFromInput(_arg1:IDataInput):void {
        this.item_ = _arg1.readInt();
        this.data_ = _arg1.readUTF();
    }

    override public function toString():String {
        return (formatToString("ITEMRESULT", "item_", "data_"));
    }

}
}//package networking.packets.server

