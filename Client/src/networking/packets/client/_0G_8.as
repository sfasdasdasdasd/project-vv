﻿




package networking.packets.client {

    import flash.utils.IDataOutput;

    public class _0G_8 extends _R_q {

    public function _0G_8(_arg1:uint) {
        super(_arg1);
    }
    public var guildName_:String;

    override public function writeToOutput(_arg1:IDataOutput):void {
        _arg1.writeUTF(this.guildName_);
    }

    override public function toString():String {
        return (formatToString("JOINGUILD", "guildName_"));
    }

}
}//package networking.packets.client

