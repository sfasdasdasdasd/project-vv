﻿




package networking.packets.client {

    import flash.utils.IDataInput;

    import networking._098;

    public class _R_q extends _098 {

    public function _R_q(_arg1:uint) {
        super(_arg1);
    }

    final override public function parseFromInput(_arg1:IDataInput):void {
        throw (new Error((("Client should not receive " + id_) + " messages")));
    }

}
}//package networking.packets.client

