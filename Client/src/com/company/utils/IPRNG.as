﻿// Decompiled by AS3 Sorcerer 1.99
// http://www.as3sorcerer.com/

//com.company.utils.IPRNG

package com.company.utils {

    import flash.utils.ByteArray;

    public interface IPRNG {

    function getPoolSize():uint;

    function init(_arg1:ByteArray):void;

    function next():uint;

    function dispose():void;

    function toString():String;

}
}//package com.hurlant.crypto.prng

