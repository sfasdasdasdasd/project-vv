﻿// Decompiled by AS3 Sorcerer 1.99
// http://www.as3sorcerer.com/

//com.company.utils.IHash

package com.company.utils {

    import flash.utils.ByteArray;

    public interface IHash {

    function getInputSize():uint;

    function getHashSize():uint;

    function hash(_arg1:ByteArray):ByteArray;

    function toString():String;

    function getPadSize():int;

}
}//package com.hurlant.crypto.hash

