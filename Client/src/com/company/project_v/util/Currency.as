﻿




package com.company.project_v.util {
public class Currency {

    public static const INVALID:int = -1;
    public static const _class:int = 0;
    public static const FAME:int = 1;
    public static const _A_h:int = 2;
    public static const SOUL:int = 3;

    public static function _01r(_arg1:int):String {
        switch (_arg1) {
            case _class:
                return ("Gold");
            case FAME:
                return ("Fame");
            case _A_h:
                return ("Guild Fame");
            case SOUL:
                return ("Souls");
        }
        return ("");
    }

}
}//package com.company.project_v.utils

