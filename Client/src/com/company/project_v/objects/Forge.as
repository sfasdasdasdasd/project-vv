package com.company.project_v.objects {

    import com.company.project_v.game.GameSprite;

    import ui.panels.Forge;
    import ui.panels.Panel;

    public class Forge extends GameObject implements _G_4 {

    public function Forge(param1:XML) {
        super(param1);
        _064 = true;
    }

    public function getPanel(param1:GameSprite):Panel {
        return new ui.panels.Forge(param1, this);
    }
}
}
