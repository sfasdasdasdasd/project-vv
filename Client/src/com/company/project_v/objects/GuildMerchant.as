﻿




package com.company.project_v.objects {

    import com.company.project_v.util.Currency;
    import com.company.project_v.util._07E_;

    import flash.display.BitmapData;

    import ui.tooltips._aS_;
    import ui.tooltips._for_;

    public class GuildMerchant extends SellableObject implements _G_4 {

    public function GuildMerchant(_arg1:XML) {
        super(_arg1);
        price_ = int(_arg1.Price);
        currency_ = Currency._A_h;
        this.description_ = _arg1.Description;
        _0F_S_ = _07E_._tS_;
    }
    public var description_:String;

    override public function soldObjectName():String {
        return (ObjectLibrary._0D_N_[objectType_]);
    }

    override public function soldObjectInternalName():String {
        var _local1:XML = ObjectLibrary._Q_F_[objectType_];
        return (_local1.@id.toString());
    }

    override public function getTooltip():_for_ {
        return (new _aS_(0x500C00, 0x9B9B9B, this.soldObjectName(), this.description_, 200));
    }

    override public function getIcon():BitmapData {
        return (ObjectLibrary.getRedrawnTextureFromType(objectType_, 80, true));
    }

}
}//package com.company.project_v.objects

