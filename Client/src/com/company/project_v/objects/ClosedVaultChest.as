﻿




package com.company.project_v.objects {

    import flash.display.BitmapData;

    import ui.tooltips._aS_;
    import ui.tooltips._for_;

    public class ClosedVaultChest extends SellableObject {

    public function ClosedVaultChest(_arg1:XML) {
        super(_arg1);
    }

    override public function soldObjectName():String {
        return ("Vault Chest");
    }

    override public function soldObjectInternalName():String {
        return ("Vault Chest");
    }

    override public function getTooltip():_for_ {
        return (new _aS_(0x500C00, 0x9B9B9B, this.soldObjectName(), ("A chest that will safely store 8 items and is " + "accessible by all of your characters."), 200));
    }

    override public function getIcon():BitmapData {
        return (ObjectLibrary.getRedrawnTextureFromType(ObjectLibrary._pb["Vault Chest"], 80, true));
    }

}
}//package com.company.project_v.objects

