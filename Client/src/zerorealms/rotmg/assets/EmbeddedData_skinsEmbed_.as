package zerorealms.rotmg.assets {

    import mx.core.ByteArrayAsset;

    [Embed(source="EmbeddedData_skinsEmbed_.dat", mimeType="application/octet-stream")]
public class EmbeddedData_skinsEmbed_ extends ByteArrayAsset {
    public function EmbeddedData_skinsEmbed_() {
        super();

    }
}
}